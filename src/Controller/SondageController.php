<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Form\MultipleVoteType;
use App\Form\SondageType;
use App\Form\VoteType;
use App\Repository\ReponseRepository;
use App\Repository\SondageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sondage")
 */
class SondageController extends AbstractController
{
    /**
     * @Route("/", name="sondage_index", methods={"GET"})
     */
    public function index(SondageRepository $sondageRepository): Response
    {
        return $this->render('sondage/index.html.twig', [
            'sondages' => $sondageRepository->findAll()
        ]);
    }

    /**
     * @Route("/user", name="sondage_user", methods={"GET"})
     */
    public function user(Request $request, SondageRepository $sondageRepository): Response
    {
        $sondages = $sondageRepository->findSondageByUser($this->getUser());

        return $this->render('sondage/user.html.twig', [
            'sondages' => $sondages
        ]);
    }

    /**
     * @Route("/new", name="sondage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sondage = new Sondage();
        $sondage->setDate(new \DateTime('now'));

        // DEFINIT PAR DEFAUT L'UTILISATEUR CONNECTE OU ANONYME
        $auteur = $this->get('security.token_storage')->getToken()->getUser();

        // RENVOIE L'UTILISATEUR DANS LE FORMULAIRE
        $sondage->setAuteur($this->getUser());

        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sondage);
            $entityManager->flush();

            $id = $sondage->getId();

            return $this->redirectToRoute('sondage_show', array("id" => $id));
        }

        return $this->render('sondage/new.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_show", methods={"GET", "POST"})
     */
    public function show(Sondage $sondage, Request $request, ReponseRepository $reponseRepository): Response
    {
        // RECUPERE LE TYPE DE REPONSE (RADIO OU CHECKBOX)
        $type = $sondage->getType();
        $id = $sondage->getId();
        $rated = 0;

        // RECUPERE LES VOTANTS
        $reponses = $reponseRepository->findUsers($this->getUser(), $sondage);

        if (!empty($reponses)) {
            $rated = 1;
        }

        // CREE LE FORMULAIRE (CHOICETYPE)
        if ($type == "radio") {
            $form = $this->createForm(VoteType::class, null, ["reponses" => $sondage->getReponse()]);
        } else {
            $form = $this->createForm(MultipleVoteType::class, null, ["reponses" => $sondage->getReponse()]);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && empty($reponses)) {
            // SI LE FORMULAIRE EST DE TYPE RADIO
            if ($type == "radio") {
                // RECUPERE LES DONNEES SELECTIONNEES
                $reponse = $form["reponses"]->getData();

                // AJOUTE A CETTE ENTREE L'ID DE L'USER
                if ($sondage->getReponse() !== $this->getUser()) {
                    $reponse->addAuteur($this->getUser());
                }

                // PERSIST LA DONNEE DANS LE TABLEAU REPONSE
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($reponse);
                $entityManager->flush();
            } else {
                // SI LE FORMULAIRE EST DE TYPE CHECKBOX
                $reponse = $form["reponses"]->getData();
                foreach ($reponse as $item) {

                    // UNIQUE || MULTIPLE ANSWERS ?
                    $item->addAuteur($this->getUser());

                    // PERSIST LA DONNEE DANS LE TABLEAU REPONSE
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($item);
                    $entityManager->flush();
                }
            }

            return $this->redirectToRoute('results', array('id' => $id));
        }

        return $this->render('sondage/show.html.twig', [
            'sondage' => $sondage,
            'type' => $type,
            'form' => $form->createView(),
            'rated' => $rated
        ]);
    }

    /**
     * @Route("/{id}/results", name="results", methods={"GET"})
     */
    public function showResults($id, ReponseRepository $reponseRepository, Sondage $sondage): Response
    {
        // RECUPERE LE TOTAL DE VOTES
        $reponse = $sondage->getReponse();
        $total = 0;

        foreach ($reponse as $item) {
            $total += count($item->getAuteur());
        }

        // ALGO COMES HERE
        return $this->render('sondage/results.html.twig', [
            'sondage' => $sondage,
            'reponse' => $reponse,
            'total' => $total
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sondage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sondage $sondage): Response
    {

        $form = $this->createForm(SondageType::class, $sondage);
        $reponse = $sondage->getReponse();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sondage_index');
        }

        return $this->render('sondage/edit.html.twig', [
            'sondage' => $sondage,
            'reponse' => $reponse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sondage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sondage $sondage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sondage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sondage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sondage_index');
    }

}
