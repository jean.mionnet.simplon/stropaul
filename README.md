# stropaul

## Présentation

Premier projet réalisé en **Symfony**: un site web de sondages avec un système de comptes.

## Installation

* Clonez le projet sur votre machine.

* En console :
    * ``` npm install ```
    * ``` composer install ``` 
    * ``` npm run dev ``` 
* Copiez le fichier **.env** et collez le à la racine du projet en un fichier **.env.local**
* Dans ce fichier **.env.local**, à la ligne 32, changez les données comme indiqué ci dessous:
    * DATABASE_URL=< dbname >://< dbuser >:< userpassword >@127.0.0.1:3306/< nomduprojet >?serverVersion=< version >

* Creez la base de données en console avec :
    * ``` bin/console d:d:c ``` 
    * ``` bin/console d:m:m ``` 
* Lancer le serveur:
    * ``` symfony serve ``` 
* VOUS ETES PRETS, ENJOY !!!

## Workflow

* figma
* Trello
* Mobile-first


*[La maquette](https://www.figma.com/file/ZuveKgK4uYgbBg5txIBs4A/stropaul?node-id=0%3A1) a été réalisée sur figma*