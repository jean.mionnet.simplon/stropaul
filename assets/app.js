import './styles/app.css';
const $ = require('jquery');

// ACTIVATION/DESACTIVATION MENU MODAL & CHANGEMENT D'ICONE
$("#toggle_menu").click(() => {
    $("#modal_menu").toggleClass('active');
    if ($("#modal_menu").hasClass('active')) {
        $("#toggle_menu").html(
            "            <svg height=\"32\" width=\"32\" version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n" +
            "                 viewBox=\"0 0 512.001 512.001\" style=\"enable-background:new 0 0 512.001 512.001;\" xml:space=\"preserve\">\n" +
            "\t\t<path fill=\"#fff\" d=\"M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717\n" +
            "\t\t\tL34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859\n" +
            "\t\t\tc-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287\n" +
            "\t\t\tl221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285\n" +
            "\t\t\tL284.286,256.002z\"/>\n" +
            "</svg>\n"
        );
    } else {
        $("#toggle_menu").html(
            "<svg id=\"menu_svg\" height=\"32\" viewBox=\"0 -53 384 384\" width=\"32\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#fff\" d=\"m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0\"/><path fill=\"#fff\" d=\"m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0\"/><path fill=\"#fff\" d=\"m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0\"/></svg>\n")
    }
})

function addTagFormDeleteLink($tagFormLi) {
    let $removeFormButton = $('<button type="button" class="remove_button">Supprimer</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        if ($("li").length > 2) {
            $tagFormLi.remove();
        };
    });
}

function addTagForm($collectionHolder, $newLinkLi) {
    let prototype = $collectionHolder.data('prototype');

    let index = $collectionHolder.data('index');
    let newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    let $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);

    addTagFormDeleteLink($newFormLi);
}

// Script pour formulaire sondage
$(document).ready(function() {

    let $collectionHolder = $('#sondage_form_reponses');
    let $addQuestionButton = $('.add_tag_link');
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addQuestionButton.on('click', function(e) {
        addTagForm($collectionHolder, $addQuestionButton);
    });

    $collectionHolder.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });

    addTagForm($collectionHolder, $addQuestionButton);
    addTagForm($collectionHolder, $addQuestionButton);
});

// GENERE DES MESSAGES ALTERNATIFS SI PAS DE SONDAGES
// PUBLIES
if ($('#brouillon_deck .sondage').length === 0) {
    $('#brouillon_deck')
        .html("<p></p><p>Aucun brouillon</p>");
}
// BROUILLONS
if  ($('#published_deck .sondage').length === 0) {
    $('#published_deck')
        .html("<p></p><p>Aucun sondage publié</p>");
}